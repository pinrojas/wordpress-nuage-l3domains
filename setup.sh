#!/bin/bash        
# Error out if no file types were provided
if [ $# -lt 3 ]
then 
  echo "Syntax Error: $0 {os-tenant-name} {os-admin-password} {nuage-l3domain-template-uuid}"
  exit 0
fi

# Checking files
if [ `echo $(ls) | grep 'wordpress.yaml' | grep 'wp-server.yaml' | grep 'get_vsd_nets_uuids.py' &> /dev/null; echo $?` == 0 ]
then 
  echo "Starting..."
else
  "ERROR: some app's files are missing... bye!"
fi

# Checking LBaaSv2
vrs_is_alive=`systemctl status openvswitch.service | grep -E "active.*since"`
lbv2_is_alive=`systemctl status neutron-lbaasv2-agent.service | grep -E "active.*since"`

if [ -z "${vrs_is_alive}" ] || [ -z "${lbv2_is_alive}" ]; then
  echo "ERROR: LBaaSv2 is not active. Check http://wp.me/p44sHI-yY for details how to activate it. bye!"
  exit -1
fi

#Preparing vars
export OS_USERNAME=admin
export OS_TENANT_NAME=admin
export OS_PASSWORD=$2
export OS_AUTH_URL=http://10.0.0.10:5000/v2.0/
export OS_REGION_NAME=RegionOne
external_network=`neutron net-external-list | grep external | sed 's/| //g' | sed 's/^\([0-9a-z\-]*\).*/\1/'`
l3_template=$3
os_admin_password=$2
os_tenant_name=$1
kystn_token=`head -20 /etc/keystone/keystone.conf | grep '^admin_token' | sed 's/^admin_token = //g'`
os_cmd_str="--os-tenant-name ${os_tenant_name}  --os-username user_${os_tenant_name} --os-password user_${os_tenant_name}"
vsd_org=OpenStack_Nuage_Lab


# Checking Keystone token
if [ -z ${kystn_token} ]
then
   echo "ERROR: Keystone's token has NOT been found into /etc/keystone/keystone.conf"
   exit -1
else
   echo "INFO: keystone's token has been found: ${kystn_token}"
fi

#Checking external network 
if [ -z ${external_network} ]
then
   echo "ERROR: External network has NOT been found."
   exit -1
else
   if [ ${#external_network} == 36 ]
   then
        echo "INFO: External network has been found: ${external_network}"
   else
        echo "ERROR: More than an etxernal has been defined. We don't know what use. Bye!"
        exit -1
   fi
fi


kystn_str="from keystoneclient.v2_0 import client\ntoken = '${kystn_token}'\nendpoint = 'http://10.0.0.10:35357/v2.0'\nkeystone = client.Client(token=token, endpoint=endpoint)\n"

#Creating tenant and user credentials 
echo "===> Step 1: Creating Tenant and User credentials..."
if [ `echo -e ${kystn_str}"print keystone.tenants.list()" | python | grep ${os_tenant_name} &> /dev/null; echo $?` == 0 ]
then
   echo "WARNING: tenant ${os_tenant_name} already exists... skipping to next step"
else
   echo -e ${kystn_str}"keystone.tenants.create(tenant_name='${os_tenant_name}', description='Default Tenant', enabled=True)" | python
   echo -e ${kystn_str}"tenants = keystone.tenants.list()\nmy_tenant = [x for x in tenants if x.name=='${os_tenant_name}'][0]\nmy_user = keystone.users.create(name='user_${os_tenant_name}', password='user_${os_tenant_name}', tenant_id=my_tenant.id)" | python
   echo -e ${kystn_str}"roles = keystone.roles.list()\nmy_role = [x for x in roles if x.name=='admin'][0]\nusers = keystone.users.list()\nmy_user = [x for x in users if x.name=='user_${os_tenant_name}'][0]\ntenants = keystone.tenants.list()\nmy_tenant = [x for x in tenants if x.name=='${os_tenant_name}'][0]\nkeystone.roles.add_user_role(my_user, my_role, my_tenant)"  | python
   echo -e ${kystn_str}"roles = keystone.roles.list()\nmy_role = [x for x in roles if x.name=='heat_stack_owner'][0]\nusers = keystone.users.list()\nmy_user = [x for x in users if x.name=='user_${os_tenant_name}'][0]\ntenants = keystone.tenants.list()\nmy_tenant = [x for x in tenants if x.name=='${os_tenant_name}'][0]\nkeystone.roles.add_user_role(my_user, my_role, my_tenant)"  | python
fi

os_tenant_id=`echo -e ${kystn_str}"tenants = keystone.tenants.list()\nmy_tenant = [x for x in tenants if x.name=='${os_tenant_name}'][0]\nprint my_tenant.id" | python`

#Creating Layer-3 Domain from Template
echo "===> Step 2: Creating Layer-3 Domain from template ${l3_template}..."
if [ `neutron router-list | grep router-${os_tenant_name} &> /dev/null; echo $?` == 0 ]
then
   echo "WARNING: Layer3-Domain already exists... skipping to next step"
else
   neutron ${os_cmd_str} router-create router-${os_tenant_name} --nuage-router-template ${l3_template}  --tenant-id ${os_tenant_id}
   neutron ${os_cmd_str} router-gateway-set router-${os_tenant_name} ${external_network}
   echo "INFO: Router router-${os_tenant_name} ...done!"
fi

#Creating instances
stck_status=`heat ${os_cmd_str} stack-list | grep  wordpress-${os_tenant_name} | sed 's/^|.*|.*| \([A-Z_]*\).*|.*|$/\1/'`

if [ -n "${stck_status}" ]
then
   if [ "${stck_status}" ==  "CREATE_FAILED" ]
   then
        echo "ERROR: Heat stack wordpress-${os_tenant_name} was previously created and it's failed. bye!"
        exit -1
   fi
fi

if [ "${stck_status}" == "CREATE_FAILED" ] || [ "${stck_status}" == "CREATE_IN_PROGRESS" ]
then
   echo "WARNING: stack wordpress-${os_tenant_name} already exist... skipping"
   sleep 15
else 
   echo "===> Step 3: creatin database, webservers and firewall instances thru heat"
   heat ${os_cmd_str} stack-create wordpress-${os_tenant_name} -f wordpress.yaml --parameters="vsd_net_partition=${vsd_org};tenant_name=${os_tenant_name}"
fi

while [ "`heat ${os_cmd_str} stack-list | grep  wordpress-${os_tenant_name} | sed 's/^|.*|.*| \([A-Z_]*\).*|.*|$/\1/'`" == "CREATE_IN_PROGRESS" ]
do
   echo "Let's wait 20 seconds for heat to end its job"
   sleep 20
done

if [ "`heat ${os_cmd_str} stack-list | grep  wordpress-${os_tenant_name} | sed 's/^|.*|.*| \([A-Z_]*\).*|.*|$/\1/'`" ==  "CREATE_FAILED" ]
then
   echo "ERROR: Heat stack wordpress-${os_tenant_name} has failed. bye!"
   exit -1
fi

echo "OK, Let's continue..."

#Creating load balancer
os_wb_id=`neutron ${os_cmd_str} subnet-list | grep ${os_tenant_name}-WebServers  | sed 's/| //g' | sed 's/^\([0-9a-z\-]*\).*/\1/'`


if [ -z "${os_wb_id}" ]
then
    echo "ERROR: OpenStack's subnet WebServers is missing.... Sorry"
    exit -1
else
    echo "===> Step 4: Creating load balancers at webservers"
    srv_web01=`nova ${os_cmd_str} list | grep wp-web01 | sed 's/ |//g' | sed 's/.*=\([0-9.]*\)$/\1/'`
    srv_web02=`nova ${os_cmd_str} list | grep wp-web02 | sed 's/ |//g' | sed 's/.*=\([0-9.]*\)$/\1/'`
    if [ -z ${srv_web01} ]
    then
        echo "ERROR: web servers are missing, bye"
        exit -1
    else
        echo "===> Step 4: Creating load balancers at webservers"
        neutron ${os_cmd_str} lbaas-loadbalancer-create --name lb-${os_tenant_name} ${os_wb_id}
        sleep 5
        neutron ${os_cmd_str} lbaas-listener-create --loadbalancer lb-${os_tenant_name} --protocol HTTP --protocol-port 80 --name lst-${os_tenant_name}
        sleep 10  # wait some seconds to get lb activebefore create pool
       	neutron ${os_cmd_str} lbaas-pool-create --lb-algorithm ROUND_ROBIN --listener lst-${os_tenant_name} --protocol HTTP --name pool-${os_tenant_name}
        neutron ${os_cmd_str} lbaas-member-create --subnet ${os_wb_id} --address ${srv_web01} --protocol-port 80 pool-${os_tenant_name}
        neutron ${os_cmd_str} lbaas-member-create --subnet ${os_wb_id} --address ${srv_web02} --protocol-port 80 pool-${os_tenant_name}
    fi 
fi

echo ""
echo "Done!"

exit 0
